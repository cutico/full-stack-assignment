﻿var screenState;
var proceed = true;

function onLoad() {

    $(window).resize(onResize);

    $("[id^=Edit-]:not([id$=-Preview])").each(function (index, element) { onChange(element) })

    onResize();

}

function onResize() {
    // If the Screenstate has changed
    var currentScreenState = getScreenType()

    if (screenState != currentScreenState) {
        console.log(currentScreenState);
        // Store the new Screenstate
        newScreenState = currentScreenState;

        // If the newScreenstate is Large or Medium, and the Old state wasn't large or Medium
        if (((newScreenState == 'xl') || (newScreenState == 'lg')) && !((screenState == 'xl') || (screenState == 'lg'))) {

            // Set some CSS (due to changes when using sm or xs screenStates)
            $("#mainBody").css('top', 0);
            $("#navSide").css('top', 0);
            // Ensure the Primary Sidebar is visible, and that all collapsible navigation boxes are collapsed.
            $("#navSide").collapse("show");
            $(".navCollapse").collapse("hide");

            // Restore Max-Height of Sidebar to 100%
            $('#navSide').css('max-height', "100%");
        }
        // if the newScreenstate is medium, small or extra-small, and was neither prior
        else if (((newScreenState == 'md') || (newScreenState == 'sm') || (newScreenState == 'xs')) && !((screenState == 'md') || (screenState == 'sm') || (screenState == 'xs'))) {

            // Collapse the Sidebar 
            $("#navSide").collapse("hide");

        }

        // Store the current ScreenState and the new ScreenState
        screenState = newScreenState;
    }

    // If the screenState is small or extra-small
    if ((screenState == 'md') || (screenState == 'sm') || (screenState == 'xs')) {

        //TODO: Remove this Code, if no issues arrise when testing on Mobile.
        // Scale the NavBar's MIT Logo to be equal in height to the Burger Button.
        //$("#navBarLogo img").css('height', $('#navBarBurger i').css('height'));
        // Move the Top of the Sidebar to be below the Navbar (Which has a dynamic height)
        $("#navSide").css('top', $("#navBar").css('height'));
        // Move the Max-Height to prevent overflow past screen edge.
        $('#navSide').css('max-height', window.innerHeight - parseInt($("#navBar").css('height')) + "px");
        // Move the Top of the Page's Mainbody below the Navbar (Which has a dynamic height)
        $("#mainBody").css('top', $("#navBar").css('height'));

    }
}

function getScreenType() {
    // Returns the Current Bootstrap ScreenType based on the Window's InnerWidth.
    // Because I couldn't find a simple way to do this with Bootstrap itself.
    var w = window.innerWidth;

    if (w < 576) {
        return 'xs';
    }
    else if (w < 768) {
        return 'sm';
    } else if (w < 992) {
        return 'md';
    } else if (w < 1200) {
        return 'lg';
    } else {
        return 'xl';
    }
}

tinymce.init({
    selector: 'textarea',
    browser_spellcheck: true,
    plugins: "lists advlist link autolink autoresize code textcolor colorpicker hr image imagetools media powerpaste contextmenu",
    toolbar: "undo redo | styleselect | bold italic underline forecolor backcolor | bullist numlist outdent indent | alignleft aligncenter alignright | insert | removeformat | code",
    spellchecker_language: 'en',
    setup: function (editor) {
        editor.on('init', function (e) {
            $("#" + editor.id + "-Preview")[0].innerHTML = editor.getContent().length > 0 ? editor.getContent() : "<i> Post Content </i>";
        });
    },
    init_instance_callback: function (editor) {
        var id = "#" + editor.id + "-Preview";
        editor.on('Change', function (e) {
            $(id)[0].innerHTML = editor.getContent().length > 0 ? editor.getContent() : "<i> Post Content </i>";
        });
        editor.on('Undo', function (e) {
            $(id)[0].innerHTML = editor.getContent().length > 0 ? editor.getContent() : "<i> Post Content </i>";
        });
        editor.on('Redo', function (e) {
            $(id)[0].innerHTML = editor.getContent().length > 0 ? editor.getContent() : "<i> Post Content </i>";
        });
    }
});

function onChange(element) {
    var previewId = "#" + element.id + "-Preview";
    if (element.id == "Edit-PostDateTime") {
        $("#blogPost-Preview").toggleClass("scheduledPost", (new Date(element.value) > new Date()));
    }
    if (element.id == "Edit-Hidden") {
        console.log("Toggling to " + element.checked)
        $("#blogPost-Preview").toggleClass("hiddenPost", element.checked);
    }
    else {

        switch (element.type) {
            case "file":
                if (element.files && element.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (file) {
                        $(previewId).attr('src', file.target.result);
                    }
                    reader.readAsDataURL(element.files[0]);
                }
            case "text":
                $(previewId)[0].innerHTML = element.value.length > 0 ? element.value : "<i>" + element.placeholder + "</i>";
                break;
            case "select-one":
                $(previewId)[0].innerHTML = element.options[element.selectedIndex].text;
                break;
            case "datetime-local":
                $(previewId)[0].innerHTML = new Date(element.value).toDateString();
        }

    }
}

function timeOut() {
    proceed = false
    setTimeout(function () {
        proceed = true
    }, 250)
}

function getUsers(searchUsers) {
    //var students;

    //var divStr = "";
    //if (searchUsers.length > 2)
    //{
    //    divStr += "<div id='beautify'>";
    //    $.ajax(
    //        {
    //            url: "/Manage/FindUsers?searchString=" + searchUsers,
    //            contentType: "json",
    //            success: function (result, status, xhr)
    //            {
    //                students = result;
    //                result = JSON.parse(result);

    //                for (i = 0; i < result.length; i++) {
    //                    divStr += "<div class='result' onclick='ReturnSearch(this.innerHTML);'> " + result[i] + "</div>";
    //                }
    //                divStr += "</div>";
    //                usersList(divStr);
    //            },
    //            error: function (xhr, status, result) {
    //                alert("Error: Bad Request");
    //            }

    //        }
    //    )
    
    //    $('#studentDisplay').autocomplete({
    //        source: students
    //    });
    //}
}

function usersList(endStr) {
    var displayResult = document.getElementById("userResult");
    displayResult.innerHTML = "";
    displayResult.innerHTML = endStr;
}

function ReturnSearch(searchUsers) {
    var userSearch = document.getElementById('userQuery');
    userSearch.value = searchUsers.substring(1);
    document.getElementById("SearchButton").click();
}