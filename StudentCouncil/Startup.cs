﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StudentCouncil.Data;
using StudentCouncil.Models;
using StudentCouncil.Services;
using Microsoft.AspNetCore.Authentication.OAuth.Claims;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using StudentCouncil.Data.Interfaces;
using StudentCouncil.Data.Repositories;
using ReflectionIT.Mvc.Paging;
using Stripe;

namespace StudentCouncil
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();


            services.AddAuthentication().AddGoogle(googleOptions =>
            {
                googleOptions.AuthorizationEndpoint += "?prompt=consent%20select_account";
                googleOptions.ClientId = Configuration["Authentication:Google:ClientId"];
                googleOptions.ClientSecret = Configuration["Authentication:Google:ClientSecret"];
                googleOptions.ClaimActions.Add(new JsonSubKeyClaimAction("urn:google:imageUrl", ClaimValueTypes.String, "image", "url"));
                googleOptions.ClaimActions.Add(new JsonSubKeyClaimAction("urn:google:imageIsDefault", ClaimValueTypes.Boolean, "image", "isDefault"));

            });
            //If we get time, see if we can make a user a current subscriber based off the time of their subscription expiring.
            //services.AddAuthorization(options => options.AddPolicy("Subscriber", policy => policy.RequireClaim("Applicable"), Convert.ToString(Convert.ToInt32(DateTime.Now) + 10000)));

            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();
            // For the Shopping cart to interact with the Orders View - Tian
            services.AddTransient<IOrderRepository, OrderRepository>();
            //Shopping Cart Related Services - Tian
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            //Allows difference Instances between Users - Tian
            services.AddScoped(sp => ShoppingCart.GetCart(sp));
            // For Paging - Tian
            services.AddPaging();
            services.AddMvc();
            //Configuring the Shopping Cart Sessions - Tian
            services.AddMemoryCache();
            services.AddSession();
            //Registers the configuration info to inject into Razor - Tian
            services.Configure<StripeSettings>(Configuration.GetSection("Stripe"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Error/400");
            }

            app.UseStaticFiles();
            // Session Configuration for Cart - Tian
            app.UseSession();
            app.UseAuthentication();

            //app.UseStatusCodePagesWithRedirects("/Error/{0}");
            app.UseStatusCodePagesWithReExecute("/Error/{0}");

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "productManaging",
                    template: "Product/{Action}", defaults: new { Controller = "Product", action="Index"
            });

                routes.MapRoute(name: "categoryFilter", template: "Product/{action}/{category}",defaults: new { Controller = "Product", action="List" });
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");


            });

            // Retrieves the secret key from the application settings and passes it to the Stripe Client Library - Tian
            StripeConfiguration.SetApiKey(Configuration.GetSection("Stripe")["SecretKey"]);
        }
    }
}
