﻿using Microsoft.AspNetCore.Mvc;
using StudentCouncil.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentCouncil.Components
{
    public class CategoryMenu : ViewComponent
    {
        private readonly ApplicationDbContext _context;
        public CategoryMenu(ApplicationDbContext context)
        {
            _context = context;
        }

        public IViewComponentResult Invoke()
        {
            var categories = _context.Categories.OrderBy(p => p.Name);
            return View(categories);
        }
    }
}
