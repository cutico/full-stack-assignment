﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentCouncil.Models.ErrorViewModels
{
    public class IndexViewModel
    {
        public string Error { get; set; }
        public List<string> Message { get; set; }
    }
}
