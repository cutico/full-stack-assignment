﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentCouncil.Models
{
    public class Category
    {
        public int ID { get; set; }
        public string Name { get; set; }
        // Allows the Entity Framework to understand that Category can have many Products
        // but Products can only have one Category. A One to Many Relationship.
        public List<Product> Products { get; set; }
    }
}
