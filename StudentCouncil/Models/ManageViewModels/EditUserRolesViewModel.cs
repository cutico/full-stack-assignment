﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StudentCouncil.Models.ManageViewModels
{
    public class EditUserRolesViewModel
    {
        public string ID { get; set; }

        [Display(Name = "Display Name")]
        public string Google_displayName { get; }
        [Display(Name = "Given Name")]
        public string Google_givenName { get; }
        [Display(Name = "Image URL")]
        public string Google_ImageURL { get; }

        public bool IsAdmin { get; set; }
        public bool IsEditor { get; set; }
        public bool IsSCMember { get; set; }

        public string StatusMessage { get; set; }
    }
}
