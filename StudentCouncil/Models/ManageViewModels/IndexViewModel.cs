﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StudentCouncil.Models.ManageViewModels
{
    public class IndexViewModel
    {
        public string Username { get; set; }

        [Display(Name = "First Name")]
        public string Google_displayName { get; set; }
        [Display(Name = "Surname")]
        public string Google_givenName { get; set; }
        [Display(Name = "Google Profile Image")]
        public string Google_ImageURL { get; set; }
        //public List<string> UserRoles { get; set; }
        public string SelRoles { get; set; }//String that concats all user roles
        public string StatusMessage { get; set; }
    }
}
