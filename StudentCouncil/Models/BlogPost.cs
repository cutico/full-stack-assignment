﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using StudentCouncil.Models;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudentCouncil.Models
{
    public class BlogPost
    {
        public int ID { get; set; }
        [NotMapped]
        public IFormFile Image { get; set; }
        [StringLength(64)]
        public string Title { get; set; }
        [StringLength(450)]
        public string AuthorID { get; set; }
        public DateTime PostDateTime { get; set; }
        public string Content { get; set; }
        public bool Hidden { get; set; }

        public ApplicationUser Author { get; set; }
    }
}
