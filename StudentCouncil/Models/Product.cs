﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace StudentCouncil.Models
{
    public class Product
    {
        public int ID { get; set; }
        [StringLength(64)]
        public string Name { get; set; }
        public string Description { get; set; }
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }
        [NotMapped]
        public IFormFile Image { get; set; }
        public bool InStock { get; set; }
        //Acts as the Foreign Key for Category.
        public int CategoryID { get; set; }

        public virtual Category Category { get; set; }


    }
}
