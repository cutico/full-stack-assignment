﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using StudentCouncil.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentCouncil.Models
{
    public class ShoppingCart
    {
        private readonly ApplicationDbContext _context;
        public ShoppingCart(ApplicationDbContext context)
        {
            _context = context;
        }

        public string ShoppingCartID { get; set; }
        public List<ShoppingCartItem> ShoppingCartItems { get; set; }
        // Implement the Shopping Cart using the Session ID - Tian
        // Returns a Shopping Cart witch contains our context and cartID session. - Tian
        public static ShoppingCart GetCart(IServiceProvider services)
        {
            ISession session = services.GetRequiredService<IHttpContextAccessor>()?
                .HttpContext.Session;
            var context = services.GetService<ApplicationDbContext>();
            string cartID = session.GetString("CartID") ?? Guid.NewGuid().ToString();
            session.SetString("CartID", cartID);

            return new ShoppingCart(context)
            {
                ShoppingCartID = cartID
            };
        }
        // Method for Adding Items to the Cart - Tian
        public void AddToCart(Product product, int amount)
        {
            var shoppingCartItem = _context.ShoppingCartItems.SingleOrDefault(
                s => s.Product.ID == product.ID && s.ShoppingCartID == ShoppingCartID);
            //If we have null add 1 - Tian
            if (shoppingCartItem == null)
            {
                shoppingCartItem = new ShoppingCartItem
                {
                    ShoppingCartID = ShoppingCartID,
                    Product = product,
                    Amount = 1
                };
                _context.ShoppingCartItems.Add(shoppingCartItem);
            }
            //If already have, increment by 1 - Tian
            else
            {
                shoppingCartItem.Amount++;
            }
            _context.SaveChanges();
        }
        //Remove from Cart Function -Tian
        public int RemoveFromCart(Product product)
        {
            var shoppingCartItem = _context.ShoppingCartItems.SingleOrDefault(
                s => s.Product.ID == product.ID && s.ShoppingCartID == ShoppingCartID);

            var LocalAmount = 0;

            if(shoppingCartItem != null)
            {
                //If amount more than 1, reduce by 1 - Tian
                if(shoppingCartItem.Amount > 1)
                {
                    shoppingCartItem.Amount--;
                    LocalAmount = shoppingCartItem.Amount;
                }
                //If only 1, remove the item - Tian
                else
                {
                    _context.ShoppingCartItems.Remove(shoppingCartItem);
                }
            }
            _context.SaveChanges();

            return LocalAmount;
        }

        //Get Shopping Cart Function which returns all the shopping cart items - Tian
        public List<ShoppingCartItem> GetShoppingCartItems()
        {
            return ShoppingCartItems ?? (ShoppingCartItems = _context.ShoppingCartItems.Where(c => c.ShoppingCartID == ShoppingCartID)
                .Include(s => s.Product)
                .ToList());
        }

        //Method for clearing the entire shopping cart
        public void ClearCart()
        {
            var cartItems = _context.ShoppingCartItems.Where(cart => cart.ShoppingCartID == ShoppingCartID);

            _context.ShoppingCartItems.RemoveRange(cartItems);
            _context.SaveChanges();
        }

        //Method for Calculating the Shopping Cart Total
        public decimal GetShoppingCartTotal()
        {
            var total = _context.ShoppingCartItems.Where(c => c.ShoppingCartID == ShoppingCartID)
                .Select(c => c.Product.Price * c.Amount).Sum();

            return total;
        }

    }
}
