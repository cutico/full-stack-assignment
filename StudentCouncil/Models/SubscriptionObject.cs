﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentCouncil.Models
{
    public class SubscriptionObject
    {
        public int Id { get; set; }
        public string StudentId { get; set; }
        public DateTime DateExpire { get; set; }
        public bool Subscribed { get; set; }
    }
}
