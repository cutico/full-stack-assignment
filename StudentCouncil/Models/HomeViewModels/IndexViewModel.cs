﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentCouncil.Models.HomeViewModels
{
    public class IndexViewModel
    {
        public List<BlogPost> Posts { get; set; }
        public List<Product> Products { get; set; }
        public bool showSubscriptionModal { get; set; }
    }
}
