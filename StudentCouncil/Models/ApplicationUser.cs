﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudentCouncil.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        private static string defaultAvatar = "~/images/DefaultAvatar.png";

        public string Google_displayName { get; set; }
        public string Google_givenName { get; set; }
        private string google_imageURL;
        public string Google_ImageURL
        {
            get
            {
                if (String.IsNullOrEmpty(this.google_imageURL))
                {

                    return defaultAvatar;
                }
                else return google_imageURL;
            }
            set
            {
                google_imageURL = value == "~/images/DefaultAvatar.png" ? null : value;
            }
        }
    }
}
