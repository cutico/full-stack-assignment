﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentCouncil.Models
{
    public class OrderDetail
    {
        public int OrderDetailID { get; set; }
        public int OrderID { get; set; }
        public int ProductID { get; set; }
        public int Amount { get; set; }
        public decimal Price { get; set; }
        // These two properties allows the entity framework to understand
        // the realtion between an order and an order detail - Tian
        public virtual Product Product { get; set; }
        public virtual Order Order { get; set; }
    }
}
