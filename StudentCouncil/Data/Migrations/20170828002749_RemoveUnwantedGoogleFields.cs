﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace StudentCouncil.Data.Migrations
{
    public partial class RemoveUnwantedGoogleFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Google_URL",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Google_familyName",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Google_id",
                table: "AspNetUsers");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Google_URL",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Google_familyName",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Google_id",
                table: "AspNetUsers",
                nullable: true);
        }
    }
}
