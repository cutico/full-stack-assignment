﻿using StudentCouncil.Data.Interfaces;
using StudentCouncil.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentCouncil.Data.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly ShoppingCart _shoppingCart;

        public OrderRepository(ApplicationDbContext context, ShoppingCart shoppingCart)
        {
            _context = context;
            _shoppingCart = shoppingCart;
        }

        public void CreateOrder(Order order)
        {
            //Sets the Time for the Order to time the Order is Made - Tian
            order.OrderPlaced = DateTime.Now;
            //Adds the Order to the Order Table - Tian
            _context.Orders.Add(order);

            // Here we are grabbing all the shoping cart items - Tian
            var shoppingCartItems = _shoppingCart.ShoppingCartItems;

            //Here we are creating a OrderDetail for each item inside the ShoppingCartItems - Tian
            foreach(var item in shoppingCartItems)
            {
                var orderDetail = new OrderDetail()
                {
                    Amount = item.Amount,
                    ProductID = item.Product.ID,
                    OrderID = order.OrderID,
                    Price = item.Product.Price
                };
                _context.OrderDetails.Add(orderDetail);
            }
            _context.SaveChanges();
        }
    }
}
