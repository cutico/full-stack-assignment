﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using StudentCouncil.Data;
using StudentCouncil.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore.Query;

namespace StudentCouncil.Controllers
{
    public class BlogController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHostingEnvironment _hostingEnvironment;

        private const int PostsPerPage = 3;

        public BlogController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _userManager = userManager;
            _hostingEnvironment = hostingEnvironment;
        }

        // GET: BlogPosts
        public IActionResult Index(int page = 1)
        {
            IQueryable<BlogPost> BlogDBContext;

            if (page < 1)
            {
                return BadRequest();
            }

            if (!User.IsInRole("Editor"))
            {
                BlogDBContext = _context.BlogPosts.Where(post => post.Hidden == false && post.PostDateTime <= DateTime.Now);
            }
            else
            {
                BlogDBContext = _context.BlogPosts;
            }
            ViewData["NextPage"] = BlogDBContext.Count() > page * PostsPerPage;
            ViewData["NumPages"] = (BlogDBContext.Count() - 1) / PostsPerPage + 1;
            ViewData["PrevPage"] = page > 1;
            ViewData["Page"] = page;

            BlogDBContext = BlogDBContext.Include(b => b.Author).OrderByDescending(Post => Post.PostDateTime).Skip((page - 1) * PostsPerPage).Take(PostsPerPage);

            if (BlogDBContext.Count() < 1)
            {
                return NotFound();
            }

            return View(BlogDBContext);
        }

        // GET: BlogPosts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            IQueryable<BlogPost> BlogDBContext = _context.BlogPosts.Include(b => b.Author);

            if (!User.IsInRole("Editor"))
            {
                BlogDBContext = BlogDBContext.Where(post => post.Hidden == false && post.PostDateTime <= DateTime.Now);
            }
            BlogDBContext = BlogDBContext.OrderByDescending(Post => Post.PostDateTime);

            var blogPost = await BlogDBContext.SingleOrDefaultAsync(m => m.ID == id);
            if (blogPost == null)
            {
                return NotFound();
            }

            var PostList = BlogDBContext.ToList();
            var nextPostID = PostList.TakeWhile(post => post.ID != blogPost.ID);
            if (nextPostID.Count() > 0)
            {
                ViewData["NextID"] = nextPostID.Last().ID;
            }
            var prevPostID = PostList.SkipWhile(post => post.ID != blogPost.ID);
            if (prevPostID.Count() > 1)
            {
                ViewData["PrevID"] = prevPostID.Skip(1).First().ID;
            }


            if (!User.IsInRole("Editor"))
            {
                if (blogPost.Hidden || blogPost.PostDateTime > DateTime.Now) return NotFound();
            }

            return View(blogPost);
        }

        // GET: BlogPosts/Create
        [Authorize(Roles = "Editor")]
        public async Task<IActionResult> Create()
        {
            ViewData["AuthorID"] = new SelectList(await _userManager.GetUsersInRoleAsync("Student_Council_Member"), "Id", "Google_displayName", (await _userManager.GetUserAsync(User)).Id);
            return View();
        }

        // POST: BlogPosts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Editor")]
        public async Task<IActionResult> Create([Bind("ID,Image,Title,AuthorID,PostDateTime,Content,Hidden")] BlogPost blogPost)
        {
            if (ModelState.IsValid)
            {

                _context.Add(blogPost);
                await _context.SaveChangesAsync();

                if (blogPost.Image.Length > 0)
                {
                    using (var stream = new FileStream($"{_hostingEnvironment.WebRootPath}\\images\\Blog\\" + blogPost.ID + ".png", FileMode.Create))
                    {
                        await blogPost.Image.CopyToAsync(stream);
                    }
                }

                return RedirectToAction(nameof(Index));
            }
            ViewData["AuthorID"] = new SelectList(await _userManager.GetUsersInRoleAsync("Student_Council_Member"), "Id", "Google_displayName", (await _userManager.GetUserAsync(User)).Id);
            return View(blogPost);
        }

        // GET: BlogPosts/Edit/5
        [Authorize(Roles = "Editor")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var blogPost = await _context.BlogPosts.SingleOrDefaultAsync(m => m.ID == id);
            if (blogPost == null)
            {
                return NotFound();
            }
            ViewData["AuthorID"] = new SelectList(await _userManager.GetUsersInRoleAsync("Student_Council_Member"), "Id", "Google_displayName", (await _userManager.GetUserAsync(User)).Id);
            return View(blogPost);
        }

        // POST: BlogPosts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Editor")]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Title,AuthorID,PostDateTime,Content,Hidden,Image")] BlogPost blogPost)
        {
            if (id != blogPost.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(blogPost);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BlogPostExists(blogPost.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                if (blogPost.Image != null)
                {
                    if (blogPost.Image.Length > 0)
                    {
                        using (var stream = new FileStream($"{_hostingEnvironment.WebRootPath}\\images\\Blog\\" + blogPost.ID + ".png", FileMode.Create))
                        {
                            await blogPost.Image.CopyToAsync(stream);
                        }
                    }
                }


                return RedirectToAction(nameof(Index));
            }
            ViewData["AuthorID"] = new SelectList(await _userManager.GetUsersInRoleAsync("Student_Council_Member"), "Id", "Google_displayName", (await _userManager.GetUserAsync(User)).Id);
            return View(blogPost);
        }

        // GET: BlogPosts/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var blogPost = await _context.BlogPosts
                .Include(b => b.Author)
                .SingleOrDefaultAsync(m => m.ID == id);
            if (blogPost == null)
            {
                return NotFound();
            }

            return View(blogPost);
        }

        // POST: BlogPosts/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var blogPost = await _context.BlogPosts.SingleOrDefaultAsync(m => m.ID == id);

            new FileInfo($"{_hostingEnvironment.WebRootPath}\\images\\Blog\\" + blogPost.ID + ".png").Delete();

            _context.BlogPosts.Remove(blogPost);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BlogPostExists(int id)
        {
            return _context.BlogPosts.Any(e => e.ID == id);
        }
    }
}
