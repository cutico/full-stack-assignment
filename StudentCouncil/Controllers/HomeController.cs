﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using StudentCouncil.Models;
using StudentCouncil.Data;
using Microsoft.EntityFrameworkCore;
using StudentCouncil.Models.HomeViewModels;

namespace StudentCouncil.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index(bool showSubscriptionModal = false)
        {
            IQueryable<BlogPost> BlogDBContext = _context.BlogPosts.Include(b => b.Author);

            if (!User.IsInRole("Editor"))
            {
                BlogDBContext = BlogDBContext.Where(post => post.Hidden == false && post.PostDateTime <= DateTime.Now);
            }
            BlogDBContext = BlogDBContext.OrderByDescending(Post => Post.PostDateTime);

            return View(new IndexViewModel() {
                Posts = BlogDBContext.Take(3).ToList(),
                Products = _context.Products.OrderBy(x => new Random().Next()).Take(3).ToList(),
                showSubscriptionModal = showSubscriptionModal
            });
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult Contact()
        {
            return View();
        }


    }
}
