﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using StudentCouncil.Models;
using Stripe;
using StudentCouncil.Data;
using Microsoft.AspNetCore.Identity;

namespace StudentCouncil.Controllers
{
    public class PaymentsController : Controller
    {
        private readonly ShoppingCart _shoppingCart;
        private readonly ApplicationDbContext _dbContext;
        private readonly UserManager<ApplicationUser> _userManager;

        public PaymentsController(ShoppingCart shoppingCart, ApplicationDbContext dbContext, UserManager<ApplicationUser> userManager)
        {
            _shoppingCart = shoppingCart;
            _dbContext = dbContext;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Charge(string stripeToken)
        {
            var items = _shoppingCart.GetShoppingCartItems();
            _shoppingCart.ShoppingCartItems = items;

            //Here we check if the shopping cart actually has any items in it. - Tian
            if (_shoppingCart.ShoppingCartItems.Count == 0)
            {
                ModelState.AddModelError("", "Your Cart is empty, please add some products first.");
            }

            var total = _shoppingCart.GetShoppingCartTotal();

            var customers = new StripeCustomerService();
            var charges = new StripeChargeService();


            var customer = customers.Create(new StripeCustomerCreateOptions
            {
                Email = User.Identity.Name,
                SourceToken = stripeToken
            });

            var charge = charges.Create(new StripeChargeCreateOptions
            {
                Amount = (int)((_dbContext.SubscriptionObject.Where(so => so.StudentId == _userManager.GetUserId(User) && so.DateExpire >= DateTime.Now).Count() > 1 ? 0.88m : 1m) * (total * 100)),
                CustomerId = customer.Id,
                Description = "Purchase of MIT Merchandise.",
                Currency = "NZD",
                ReceiptEmail = customer.Email,

            });

            _shoppingCart.ClearCart();

            return View();
        }
    }
}