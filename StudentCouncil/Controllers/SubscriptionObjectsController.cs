﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StudentCouncil.Data;
using StudentCouncil.Models;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Stripe;

namespace StudentCouncil.Controllers
{
    public class SubscriptionObjectsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public SubscriptionObjectsController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;

        }

        // GET: SubscriptionObjects
        public IActionResult Index()
        {
            return View(/*await _context.SubscriptionObject.ToListAsync()*/);
        }


        public IActionResult SetSubscription(SubscriptionObject subscribe, string stripeToken)
        {
            var sub = new SubscriptionObject
            {
                Id = subscribe.Id,
                StudentId = _userManager.GetUserId(User),
                DateExpire = DateTime.Now.AddYears(1),
                // For testing purposes comment the above line of code, and uncomment the below code.
                //DateExpire = DateTime.Now.AddMinutes(1),
                Subscribed = true
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.SubscriptionObject.Add(sub);
            _context.SaveChanges();


            var customers = new StripeCustomerService();
            var charges = new StripeChargeService();

            var customer = customers.Create(new StripeCustomerCreateOptions
            {
                Email = User.Identity.Name,
                SourceToken = stripeToken
            });

            var charge = charges.Create(new StripeChargeCreateOptions
            {
                Amount = 1500,
                Description = "Premium Subscription",
                Currency = "nzd",
                CustomerId = customer.Id
            });

            return RedirectToAction("Index", "Product");
        }


        private bool SubscriptionObjectExists(int id)
        {
            return _context.SubscriptionObject.Any(e => e.Id == id);
        }
    }
}
