﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using StudentCouncil.Data;
using StudentCouncil.Models;
using StudentCouncil.Models.ShoppingCartViewModels;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace StudentCouncil.Controllers
{
    public class ShoppingCartController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly ShoppingCart _shoppingCart;
        //Constructor
        public ShoppingCartController(ApplicationDbContext context, ShoppingCart shoppingCart)
        {
            _context = context;
            _shoppingCart = shoppingCart;
        }

        public ViewResult Index()
        {
            var items = _shoppingCart.GetShoppingCartItems();
            _shoppingCart.ShoppingCartItems = items;

            var scvm = new ShoppingCartViewModel
            {
                ShoppingCart = _shoppingCart,
                ShoppingCartTotal = _shoppingCart.GetShoppingCartTotal()
            };

            return View(scvm);
        }
        [Authorize]
        //View to confirm adding a item - Tian
        public RedirectToActionResult AddToShoppingCart(int ID)
        {
            var selectedProduct = _context.Products.FirstOrDefault(p => p.ID == ID);
            if(selectedProduct != null)
            {

                _shoppingCart.AddToCart(selectedProduct, 1);
            }
            return RedirectToAction("Index");
        }
        //View to confirm removing an item - Tian
        public RedirectToActionResult RemoveFromShoppingCart(int ID)
        {
            var selectedProduct = _context.Products.FirstOrDefault(p => p.ID == ID);
            if(selectedProduct != null)
            {
                _shoppingCart.RemoveFromCart(selectedProduct);
            }
            return RedirectToAction("Index");
           
        }
    }
}
