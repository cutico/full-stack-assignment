﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Mvc;
//using StudentCouncil.Data.Interfaces;
//using StudentCouncil.Models;
//using Microsoft.AspNetCore.Authorization;
//using Stripe;

//namespace StudentCouncil.Controllers
//{
//    public class OrderController : Controller
//    {
//        private readonly IOrderRepository _orderRepository;
//        private readonly ShoppingCart _shoppingCart;

//        public OrderController(IOrderRepository orderRepository, ShoppingCart shoppingCart)
//        {
//            _orderRepository = orderRepository;
//            _shoppingCart = shoppingCart;
//        }
//        [Authorize]
//        public IActionResult Checkout()
//        {
//            return View();
//        }
//        //Distinguish the difference between these two - Tian
//        [HttpPost]
//        [Authorize]
//        public IActionResult Checkout(Order order)
//        {
//            var items = _shoppingCart.GetShoppingCartItems();
//            _shoppingCart.ShoppingCartItems = items;

//            //Here we check if the shopping cart actually has any items in it. - Tian
//            if(_shoppingCart.ShoppingCartItems.Count == 0)
//            {
//                ModelState.AddModelError("", "Your Cart is empty, please add some products first.");
//            }

//            //This 'if' statement will check if the ModelState is valid, like the validations
//            // which we can add later - Tian
//            if(ModelState.IsValid)
//            {
//                _orderRepository.CreateOrder(order);
//                _shoppingCart.ClearCart();
//                return RedirectToAction("CheckoutComplete");
//            }
//            return View(order);
//        }
//        public IActionResult CheckoutComplete()
//        {
//            ViewBag.CheckoutCompleteMessage = "Your Payment was Successful";
//            return View();
//        }


//        // STRIPE CHARGE METHOD
//        public IActionResult Charge(string stripeToken)
//        {
//            var items = _shoppingCart.GetShoppingCartItems();
//            _shoppingCart.ShoppingCartItems = items;

//            //Here we check if the shopping cart actually has any items in it. - Tian
//            if (_shoppingCart.ShoppingCartItems.Count == 0)
//            {
//                ModelState.AddModelError("", "Your Cart is empty, please add some products first.");
//            }

//            var total = _shoppingCart.GetShoppingCartTotal();

//            var customers = new StripeCustomerService();
//            var charges = new StripeChargeService();


//            var customer = customers.Create(new StripeCustomerCreateOptions
//            {
//                Email = User.Identity.Name,
//                SourceToken = stripeToken
//            });

//            var charge = charges.Create(new StripeChargeCreateOptions
//            {
//                Amount = (int)(total * 100),
//                CustomerId = customer.Id,
//                Description = "Purchase of MIT Merchandise.",
//                Currency = "NZD",
//                ReceiptEmail = customer.Email,

//            });

//            _shoppingCart.ClearCart();

//            return View();
//        }
//    }
//}