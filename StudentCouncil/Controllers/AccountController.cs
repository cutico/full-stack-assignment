﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using StudentCouncil.Models;
using StudentCouncil.Services;
using System.Text.RegularExpressions;
using StudentCouncil.Data;

namespace StudentCouncil.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ApplicationDbContext _context;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private readonly Regex AccountDomainCheck;

        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ApplicationDbContext context,
            IEmailSender emailSender,
            ILogger<AccountController> logger)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
            _emailSender = emailSender;
            _logger = logger;
            AccountDomainCheck = new Regex(@"^[a-zA-Z0-9.]+@manukau(mail\.com|\.ac\.nz)$");
        }

        [TempData]
        public string ErrorMessage { get; set; }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Lockout()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            _logger.LogInformation("User logged out.");
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login(string returnUrl = null)
        {
            // Request a redirect to the external login provider.
            string redirectUrl = Url.Action(nameof(LoginCallback), "Account", new { returnUrl });
            AuthenticationProperties properties = _signInManager.ConfigureExternalAuthenticationProperties("Google", redirectUrl);
            ChallengeResult challenge = Challenge(properties, "Google");
            return challenge;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> LoginCallback(string returnUrl = null, string remoteError = null)
        {
            if (remoteError != null)
            {
                ErrorMessage = $"Error from external provider: {remoteError}";
                return RedirectToAction(returnUrl);
            }
            var info = await _signInManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return RedirectToAction(returnUrl);
            }

            //_userManager.FindByLoginAsync(info.LoginProvider, info.ProviderKey).Result.G
            // Sign in the user with this external login provider if the user already has an identity.
            var result = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false, bypassTwoFactor: true);
            if (result.Succeeded)
            {
                ApplicationUser user = await _userManager.FindByLoginAsync(info.LoginProvider, info.ProviderKey);

                bool updateNeeded = false;

                var displayName = info.Principal.FindFirstValue(ClaimTypes.Name);
                if (user.Google_displayName != displayName) { user.Google_displayName = displayName; updateNeeded = true; }

                var givenName = info.Principal.FindFirstValue(ClaimTypes.GivenName);
                if (user.Google_givenName != givenName) { user.Google_givenName = givenName; updateNeeded = true; }

                var isDefault = info.Principal.FindFirstValue("urn:google:imageIsDefault");
                var imgUrl = (isDefault == "False" ? info.Principal.FindFirstValue("urn:google:imageUrl") : null);
                if (user.Google_ImageURL != imgUrl) { user.Google_ImageURL = imgUrl; updateNeeded = true; }

                if (updateNeeded) await _userManager.UpdateAsync(user);

                _logger.LogInformation("User logged in with {Name} provider.", info.LoginProvider);

                var subscription = _context.SubscriptionObject.Where(so => so.StudentId == user.Id && so.Subscribed == true);
                if (subscription.First().DateExpire < DateTime.Now)
                {
                    return RedirectToAction("Index","Home", new { showSubscriptionModal = true});
                }

                return RedirectToLocal(returnUrl);
            }
            if (result.IsLockedOut)
            {
                return RedirectToAction(nameof(Lockout));
            }
            else
            {
                var email = info.Principal.FindFirstValue(ClaimTypes.Email);

                if (!AccountDomainCheck.IsMatch(email))
                {
                    AddErrors(IdentityResult.Failed());

                    ViewData["ReturnUrl"] = returnUrl;
                    //return StatusCode(401, "Authentication Denied: Please ensure you are logging in with a Manukaumail.com or Manukau.ac.nz email address");
                    return Unauthorized();
                    //return RedirectToAction("401", "Error");
                }

                var displayName = info.Principal.FindFirstValue(ClaimTypes.Name);
                var givenName = info.Principal.FindFirstValue(ClaimTypes.GivenName);
                var imgUrl = info.Principal.FindFirstValue("urn:google:imageUrl");
                var isDefault = info.Principal.FindFirstValue("urn:google:imageIsDefault");

                var user = new ApplicationUser { UserName = email, Email = email, Google_displayName = displayName, Google_givenName = givenName, Google_ImageURL = (isDefault == "False" ? imgUrl : null) };
                var loginresult = await _userManager.CreateAsync(user);
                if (loginresult.Succeeded)
                {
                    loginresult = await _userManager.AddLoginAsync(user, info);
                    if (loginresult.Succeeded)
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        _logger.LogInformation("User created an account using {Name} provider.", info.LoginProvider);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(loginresult);

                ViewData["ReturnUrl"] = returnUrl;
                return RedirectToLocal(returnUrl);
            }
        }

        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        #endregion
    }
}
