﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using StudentCouncil.Models.ErrorViewModels;
using System.Net;

namespace StudentCouncil.Controllers
{
    public class ErrorController : Controller
    {
        //[HttpGet("/Error/{error}")]
        //public IActionResult Index(int error) => Index(error.ToString());

        Dictionary<String, List<String>> errorDict = new Dictionary<String, List<String>>()
        {
            ["400"] = new List<String> { "Bad Request", "The server cannot or will not process the request.","If the issue persists, please contact the Site Admins." },
            ["401"] = new List<String> { "Unauthorized", "Authorization has Failed.","Please ensure you are logging in using a MIT Email Address ending in either 'Manukaumail.com' or 'Manukau.ac.nz'." },
            ["403"] = new List<String> { "Forbidden", "You are trying to reach an area of the site that you do not have access to.","Please ensure you are correctly logged in.","If you believe this is in error, please contact the Site Admins." },
            ["404"] = new List<String> { "Not Found", "Nothing to see here...","If you've been taken here by a Link on the site, please send an email to the Site Admins." }
        };

        [HttpGet("/Error/{error}")]
        [HttpPost("/Error/{error}")]
        public IActionResult Index(string error, List<string> message)
        {
            if (String.IsNullOrEmpty(error)) error = "400";
                      
            if (message.Count == 0 && errorDict.ContainsKey(error))
            {
                message = errorDict[error];
            }

            var model = new IndexViewModel()
            {
                Error = error,

                Message = message
            };

            return View(model);
        }
    }
}