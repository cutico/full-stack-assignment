﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using StudentCouncil.Models;
using StudentCouncil.Models.ManageViewModels;
using StudentCouncil.Services;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;

namespace StudentCouncil.Controllers
{    //IMPORTANT NOTE, MAY NEED TO ALLOW ANONYMOUS ACCESS TO THIS CONTROLLER AND THEN RESTRICT WHAT WE DON'T WANT USERS TO SEE TO ADMIN ONLY, ANNOYING BUT OTHERWISE DOESN'T WORK

    [Authorize(Roles = "Admin")]
    [Route("[controller]/[action]")]
    public class ManageController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private readonly UrlEncoder _urlEncoder;

        public ManageController(
          UserManager<ApplicationUser> userManager,
          SignInManager<ApplicationUser> signInManager,
          RoleManager<IdentityRole> roleManager,
          IEmailSender emailSender,
          ILogger<ManageController> logger,
          UrlEncoder urlEncoder)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _emailSender = emailSender;
            _logger = logger;
            _urlEncoder = urlEncoder;
        }

        [TempData]
        public string StatusMessage { get; set; }
        public List<IdentityRole> RoleList { get; private set; }
        public string SelRoles { get; set; }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return Unauthorized();
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            RoleList = _roleManager.Roles.ToList();
            bool numRole = false;
            foreach (IdentityRole role in RoleList)
            {

                string convStr = Convert.ToString(role);


                if (User.IsInRole(convStr)) 
                {
                    if (!numRole)
                    {
                        SelRoles += convStr;
                        numRole = true;
                    }
                    else
                    {
                        SelRoles += ", " + convStr;
                    }
                }  
            }


            var model = new IndexViewModel
            {
                Username = user.UserName,
                Google_displayName = user.Google_displayName,
                Google_givenName = user.Google_givenName,
                Google_ImageURL = user.Google_ImageURL,
                StatusMessage = StatusMessage,
                SelRoles = SelRoles
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Index(IndexViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            StatusMessage = "Your profile has been updated";
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public async Task<IActionResult> ManageUsers()
        {
            var DefaultResults = new List<ApplicationUser>();
            DefaultResults.AddRange(await _userManager.GetUsersInRoleAsync("Admin"));
            DefaultResults.AddRange(await _userManager.GetUsersInRoleAsync("Editor"));
            DefaultResults.AddRange(await _userManager.GetUsersInRoleAsync("Student_Council_Member"));
            return View(DefaultResults.Distinct().ToList());
        }

        [HttpPost]
        public async Task<ActionResult> ManageUsers(string searchUsers)
        {
            var members = from m in _userManager.Users //Query
                          select m;
            var loopMem = members;
            if (!String.IsNullOrEmpty(searchUsers))
            {
                members = members.Where(s => s.Google_displayName.Contains(searchUsers));
            }
            if (!members.Any())
            {
                members = loopMem;

                //Would like to display an error message to the user at this stage.
            }
            return View(await members.ToListAsync());
        }

        //Search function for finding members by name
        [HttpGet]
        public string FindUsers(string term = null)
        {
            var members = from m in _userManager.Users //Query
                          select m.Google_displayName;

            if (!String.IsNullOrEmpty(term))
            {
                members = members.Where(s => s.Contains(term));
            }

            return JsonConvert.SerializeObject(members);
        }
        //End search function

        [HttpGet]
        public async Task<IActionResult> EditUserRoles(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{id}'.");
            }

            var model = new EditUserRolesViewModel
            {
                ID = id,
                IsAdmin = await _userManager.IsInRoleAsync(user, "Admin"),
                IsEditor = await _userManager.IsInRoleAsync(user, "Editor"),
                IsSCMember = await _userManager.IsInRoleAsync(user, "Student_Council_Member")
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditUserRoles(string id, EditUserRolesViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (model.IsAdmin != await _userManager.IsInRoleAsync(user, "Admin"))
            {
                if (model.IsAdmin)
                {
                    await _userManager.AddToRoleAsync(user, "Admin");
                }
                else
                {
                    await _userManager.RemoveFromRoleAsync(user, "Admin");
                }
            }
            if (model.IsEditor != await _userManager.IsInRoleAsync(user, "Editor"))
            {
                if (model.IsEditor)
                {
                    await _userManager.AddToRoleAsync(user, "Editor");
                }
                else
                {
                    await _userManager.RemoveFromRoleAsync(user, "Editor");
                }
            }
            if (model.IsSCMember != await _userManager.IsInRoleAsync(user, "Student_Council_Member"))
            {
                if (model.IsSCMember)
                {
                    await _userManager.AddToRoleAsync(user, "Student_Council_Member");
                }
                else
                {
                    await _userManager.RemoveFromRoleAsync(user, "Student_Council_Member");
                }
            }

            StatusMessage = "Your profile has been updated";
            return RedirectToAction(nameof(ManageUsers));
        }

        //#region Helpers

        //private void AddErrors(IdentityResult result)
        //{
        //    foreach (var error in result.Errors)
        //    {
        //        ModelState.AddModelError(string.Empty, error.Description);
        //    }
        //}

        //private string FormatKey(string unformattedKey)
        //{
        //    var result = new StringBuilder();
        //    int currentPosition = 0;
        //    while (currentPosition + 4 < unformattedKey.Length)
        //    {
        //        result.Append(unformattedKey.Substring(currentPosition, 4)).Append(" ");
        //        currentPosition += 4;
        //    }
        //    if (currentPosition < unformattedKey.Length)
        //    {
        //        result.Append(unformattedKey.Substring(currentPosition));
        //    }

        //    return result.ToString().ToLowerInvariant();
        //}

        //#endregion
    }
}


