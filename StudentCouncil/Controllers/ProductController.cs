﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StudentCouncil.Data;
using StudentCouncil.Models.ProductViewModels;
using StudentCouncil.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using System.Globalization;

namespace StudentCouncil.Controllers
{
    public class ProductController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment _hostingEnvironment;

        private const int PostsPerPage = 6;

        public ProductController(ApplicationDbContext context, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
        }
        public ActionResult Index(string category, int page = 1)
        {
            //View All Categories, Apparel or Accessories - Tian
            IEnumerable<Product> products;

            if (page < 1)
            {
                return BadRequest();
            }

            string currentCategory = "";
            if (string.IsNullOrEmpty(category))
            {
                products = _context.Products.OrderBy(n => n.ID);
                currentCategory = "All Products";
            }
            else
            {
                products = _context.Products.Where(p => p.Category.Name.ToLower().Equals(category.ToLower())).OrderBy(p => p.Name);
                currentCategory = category;
            }

            ViewData["NextPage"] = products.Count() > page * PostsPerPage;
            ViewData["NumPages"] = (products.Count() - 1) / PostsPerPage + 1;
            ViewData["PrevPage"] = page > 1;
            ViewData["Page"] = page;

            if (page > (int)ViewData["NumPages"])
            {
                return RedirectToAction("Index", new { page = (int)ViewData["NumPages"] });
            }

            return View(new ProductIndexViewModel
            {
                Products = products.Skip((page - 1) * PostsPerPage).Take(PostsPerPage),
                CurrentCategory = new CultureInfo("en-US", false).TextInfo.ToTitleCase(currentCategory)
            });
        }


        // Displays the Products Index in a List  - Tian
        public async Task<IActionResult> Manage(string searchString)
        {
            return View(await _context.Products.AsNoTracking().ToListAsync());
        }
        // Displays the Details View of a singular Product - Tian
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var product = await _context.Products.Include(x => x.Category).AsNoTracking().SingleOrDefaultAsync(y => y.ID == id);
            if (product == null)
            {
                return NotFound();
            }
            return View(product);
        }
        //Get - Create 
        public IActionResult Create()
        {
            ViewBag.CategoryID = new SelectList(_context.Categories, "ID", "Name");
            return View();
        }
        //Post - Create 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,Description,Price,Image,InStock,CategoryID")] Product product)
        {
            if (ModelState.IsValid)
            {
                _context.Add(product);
                await _context.SaveChangesAsync();

                if (product.Image.Length > 0)
                {
                    using (var stream = new FileStream($"{_hostingEnvironment.WebRootPath}\\images\\Products\\" + product.ID + ".png", FileMode.Create))
                    {
                        await product.Image.CopyToAsync(stream);
                    }
                }

                return RedirectToAction(nameof(Manage));
            }
            ViewData["CategoryID"] = new SelectList(_context.Categories, "ID", "Name", product.CategoryID);
            return View(product);
        }

        // GET: Product/Edit/5
        [Authorize(Roles = "Editor")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Product = await _context.Products.SingleOrDefaultAsync(m => m.ID == id);
            if (Product == null)
            {
                return NotFound();
            }
            ViewData["CategoryID"] = new SelectList(_context.Categories, "ID", "Name", Product.CategoryID);
            return View(Product);
        }

        // POST: Product/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Editor")]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,Description,Price,Image,InStock,CategoryID")] Product Product)
        {
            if (id != Product.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(Product);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductExists(Product.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                if (Product.Image != null)
                {
                    if (Product.Image.Length > 0)
                    {
                        using (var stream = new FileStream($"{_hostingEnvironment.WebRootPath}\\images\\Products\\" + Product.ID + ".png", FileMode.Create))
                        {
                            await Product.Image.CopyToAsync(stream);
                        }
                    }
                }


                return RedirectToAction(nameof(Manage));
            }
            ViewData["CategoryID"] = new SelectList(_context.Categories, "ID", "Name", Product.CategoryID);
            return View(Product);
        }

        //// GET: Edit
        //public async Task<IActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var product = await _context.Products
        //        .AsNoTracking()
        //        .SingleOrDefaultAsync(m => m.ID == id);
        //    if (product == null)
        //    {
        //        return NotFound();
        //    }
        //    ViewBag.CategoryID = new SelectList(_context.Categories, "ID", "Name", product.CategoryID);
        //    return View(product);
        //}

        ////POST - Edit
        //[HttpPost, ActionName("Edit")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> EditPost(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var productToUpdate = await _context.Products
        //        .SingleOrDefaultAsync(p => p.ID == id);

        //    if (await TryUpdateModelAsync<Product>(productToUpdate,
        //        "",
        //        p => p.Name, p => p.Description, p => p.Price, p => p.Image, p => p.InStock, p => p.CategoryID))
        //    {
        //        try
        //        {
        //            if (productToUpdate.Image.Length > 0)
        //            {
        //                using (var stream = new FileStream($"{_hostingEnvironment.WebRootPath}\\images\\Products\\" + productToUpdate.ID + ".png", FileMode.Create))
        //                {
        //                    await productToUpdate.Image.CopyToAsync(stream);
        //                }
        //            }
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateException /* ex */)
        //        {
        //            //Log the error (uncomment ex variable name and write a log.)
        //            ModelState.AddModelError("", "Unable to save changes. " +
        //                "Try again, and if the problem persists, " +
        //                "see your administrator.");
        //        }
        //        return RedirectToAction(nameof(Index));
        //    }
        //ViewBag.CategoryID = new SelectList(_context.Categories, "ID", "Name", productToUpdate.CategoryID);
        //    return View(productToUpdate);
        //}

        //GET - Delete
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Products
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.ID == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        //POST - Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            //change this so removes from shoppingcart
            Product product = await _context.Products  
                .SingleAsync(p => p.ID == id);

            new FileInfo($"{_hostingEnvironment.WebRootPath}\\images\\Products\\" + product.ID + ".png").Delete();

            //var shoppingCartItem = await _context.ShoppingCartItems
            //    .Where(s => s.Product.ID == id)
            //    .ToListAsync();

            _context.Products.Remove(product);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Manage));
        }

        private bool ProductExists(int id)
        {
            return _context.Products.Any(e => e.ID == id);
        }
    }
}

